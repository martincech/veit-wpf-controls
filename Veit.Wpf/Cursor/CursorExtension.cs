﻿using System.Collections.Generic;
using SWI = System.Windows.Input;

namespace Veit.Wpf.Cursor
{
    public static class CursorExtension
    {
        static readonly Dictionary<SWI.CursorType, SWI.Cursor> cursorsMap = new Dictionary<SWI.CursorType, SWI.Cursor>
        {
            { SWI.CursorType.None, SWI.Cursors.None},
            { SWI.CursorType.No, SWI.Cursors.No},
            { SWI.CursorType.Arrow, SWI.Cursors.Arrow},
            { SWI.CursorType.AppStarting, SWI.Cursors.AppStarting},
            { SWI.CursorType.Cross, SWI.Cursors.Cross},
            { SWI.CursorType.Help, SWI.Cursors.Help},
            { SWI.CursorType.IBeam, SWI.Cursors.IBeam},
            { SWI.CursorType.SizeAll, SWI.Cursors.SizeAll},
            { SWI.CursorType.SizeNESW, SWI.Cursors.SizeNESW},
            { SWI.CursorType.SizeNS, SWI.Cursors.SizeNS},
            { SWI.CursorType.SizeNWSE, SWI.Cursors.SizeNWSE},
            { SWI.CursorType.SizeWE, SWI.Cursors.SizeWE},
            { SWI.CursorType.UpArrow, SWI.Cursors.UpArrow},
            { SWI.CursorType.Wait, SWI.Cursors.Wait},
            { SWI.CursorType.Hand, SWI.Cursors.Hand},
            { SWI.CursorType.Pen, SWI.Cursors.Pen},
            { SWI.CursorType.ScrollNS, SWI.Cursors.ScrollNS},
            { SWI.CursorType.ScrollWE, SWI.Cursors.ScrollWE},
            { SWI.CursorType.ScrollAll, SWI.Cursors.ScrollAll},
            { SWI.CursorType.ScrollN, SWI.Cursors.ScrollN},
            { SWI.CursorType.ScrollS, SWI.Cursors.ScrollS},
            { SWI.CursorType.ScrollW, SWI.Cursors.ScrollW},
            { SWI.CursorType.ScrollE, SWI.Cursors.ScrollE},
            { SWI.CursorType.ScrollNW, SWI.Cursors.ScrollNW},
            { SWI.CursorType.ScrollNE, SWI.Cursors.ScrollNE},
            { SWI.CursorType.ScrollSW, SWI.Cursors.ScrollSW},
            { SWI.CursorType.ScrollSE, SWI.Cursors.ScrollSE},
            { SWI.CursorType.ArrowCD, SWI.Cursors.ArrowCD}
        };

        public static SWI.Cursor Map(this SWI.CursorType type)
        {
            if (cursorsMap.ContainsKey(type))
                return cursorsMap[type];
            return null;
        }
    }
}
