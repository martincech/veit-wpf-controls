﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Veit.Wpf.Converters
{
    public class EmptyToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = System.Convert.ToInt32(value);

            if (parameter != null)
            {
                return val == 0 ? Visibility.Visible : Visibility.Collapsed;
            }
            return val == 0 ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
