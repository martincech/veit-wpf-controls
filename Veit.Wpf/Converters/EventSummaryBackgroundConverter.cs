﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Veit.Wpf.Controls.ViewModels.Calendar;
using Veit.Wpf.Resources.Dictionaries;

namespace Veit.Wpf.Converters
{
    public class EventSummaryBackgroundConverter : IValueConverter
    {
        private const Resources.Dictionaries.Brush PASSED = Resources.Dictionaries.Brush.Passed;
        private const Resources.Dictionaries.Brush SCHEDULE = Resources.Dictionaries.Brush.Schedule;
        private const Resources.Dictionaries.Brush ACTIVE = Resources.Dictionaries.Brush.AccentBrush;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !(value is TaskEventViewModel vm))
            {
                return new SolidColorBrush(Colors.Transparent);
            }

            var brush = ResolveBrush(vm);
            return BrushResolver.GetBrushFromResources(brush);
        }

        private static Resources.Dictionaries.Brush ResolveBrush(TaskEventViewModel vm)
        {
            var currentDate = DateTime.Now;
            var timeStampCompareResult = DateTime.Compare(vm.TimeStamp.Date, currentDate.Date);
            if (timeStampCompareResult > 0)
            {  //schedule
                return SCHEDULE;
            }

            if (timeStampCompareResult < 0)
            {  //passed
                return PASSED;
            }

            //current
            return ResolveBrushForRunningEvent(vm.StartHour, vm.EndHour, currentDate.Hour);
        }

        private static Resources.Dictionaries.Brush ResolveBrushForRunningEvent(int startHour, int endHour, int currentHour)
        {
            if (startHour == 0 && endHour == 0)   //if event is not whole day, resolve status
            {
                return ACTIVE;
            }
            if (currentHour < startHour)
            {  //schedule
                return SCHEDULE;
            }
            return currentHour > endHour ? PASSED : ACTIVE;
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
