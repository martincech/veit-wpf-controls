﻿using Veit.Wpf.Mvvm.Observable;
using System;

namespace Veit.Wpf.Controls.ViewModels.Calendar
{
   public class TaskEventViewModel : ObservableObject
   {
      private string title;
      private string description;
      private int startHour;
      private int endHour;
      private DateTime timeStamp;
      private EventType type;

      public string Title { get { return title; } set { SetProperty(ref title, value); } }
      public string Description { get { return description; } set { SetProperty(ref description, value); } }
      public int StartHour { get { return startHour; } set { SetProperty(ref startHour, value); } }
      public int EndHour { get { return endHour; } set { SetProperty(ref endHour, value); } }
      public DateTime TimeStamp { get { return timeStamp; } set { SetProperty(ref timeStamp, value); } }
      public EventType Type { get { return type; } set { SetProperty(ref type, value); } }
   }
}
