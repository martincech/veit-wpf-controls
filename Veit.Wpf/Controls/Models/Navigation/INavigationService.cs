﻿namespace Veit.Wpf.Controls.Models.Navigation
{
   public interface INavigationService
   {
      //
      // Summary:
      //     The key corresponding to the currently displayed page.
      NavigationItem CurrentPage { get; }

      //
      // Summary:
      //     If possible, instructs the navigation service to discard the current page and
      //     display the previous page on the navigation stack.
      void GoBack();
      bool CanGoBack { get; }

      //
      // Summary:
      //     Instructs the navigation service to display a new page corresponding to the given
      //     key. Depending on the platforms, the navigation service might have to be configured
      //     with a key/page list.
      //
      // Parameters:
      //   pageKey:
      //     The key corresponding to the page that should be displayed.
      void NavigateTo(NavigationItem page);
      //
      // Summary:
      //     Instructs the navigation service to display a new page corresponding to the given
      //     key, and passes a parameter to the new page. Depending on the platforms, the
      //     navigation service might have to be Configure with a key/page list.
      //
      // Parameters:
      //   pageKey:
      //     The key corresponding to the page that should be displayed.
      //
      //   parameter:
      //     The parameter that should be passed to the new page.
      void NavigateTo(NavigationItem page, object parameter);


      /// <summary>
      /// Gets the parameter.
      /// </summary>
      /// <value>
      /// The parameter.
      /// </value>
      object Parameter { get; }
   }
}
