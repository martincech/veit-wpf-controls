﻿namespace Veit.Wpf.Resources.Dictionaries
{
   public enum Brush
   {
      White,
      AccentBrush,
      Red,
      Black,
      Passed,
      Dark,
      HoverDark,
      Schedule,
      Background,
      TableHeaderDark,
      TableHeaderLight,
      TableRow,
      ControlBackground,
      ControlBorder,
      ControlSelectBackground,
      ControlHoverBackground,
      Error,
      FlyoutBackground,
      TableUnit,
      Warning
   }
}
