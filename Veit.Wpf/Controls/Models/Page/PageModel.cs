﻿using System;
using System.Collections.Generic;

namespace Veit.Wpf.Controls.Models.Page
{
   public class PageModel
   {
      public IEnumerable<IMenuPage> MainMenuItems { get; set; }
      public IEnumerable<IMenuPage> OptionalMenuItems { get; set; }
      public Uri BarMenu { get; set; }
      public INavigationPage DefaultDetail { get; set; }
   }
}
