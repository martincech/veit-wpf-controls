﻿using System;

namespace Veit.Wpf.Demo
{
    /// <summary>
    /// Interaction logic for DemoMiscWindow.xaml
    /// </summary>
    public partial class DemoMiscWindow
    {
        public DemoMiscWindow()
        {
            InitializeComponent();

            var currentDate = DateTime.Now;
            const int shift = 2;
            datePicker.SelectedDate = currentDate;
            datePicker.DisplayDateStart = currentDate.AddMonths(-1 * shift);
            datePicker.DisplayDateEnd = currentDate.AddMonths(shift);
        }
    }
}
