﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Veit.Wpf.Controls.Views.DataGrid
{
    public class DataGridTouchOptimized : System.Windows.Controls.DataGrid
    {
        public static readonly DependencyProperty SelectedItemChangedCommandProperty = DependencyProperty.Register(nameof(SelectedItemChangedCommand), typeof(ICommand), typeof(DataGridTouchOptimized), new PropertyMetadata(null));

        private ScrollViewer sv;
        private Point oldMousePosition;
        private bool wasMoved;

        static DataGridTouchOptimized()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DataGridTouchOptimized), new FrameworkPropertyMetadata(typeof(DataGridTouchOptimized)));
        }

        public DataGridTouchOptimized()
        {
            Loaded += DataGridTouchOptimized_Loaded;
            Unloaded += DataGridTouchOptimized_Unloaded;
            MouseMove += DataGridTouchOptimized_MouseMove;
            MouseLeftButtonUp += DataGridTouchOptimized_MouseLeftButtonUp;
        }

        public ICommand SelectedItemChangedCommand
        {
            get { return (ICommand)GetValue(SelectedItemChangedCommandProperty); }
            set { SetValue(SelectedItemChangedCommandProperty, value); }
        }

        public static ScrollViewer GetScrollViewer(UIElement element)
        {
            if (element == null) return null;

            ScrollViewer retour = null;
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(element) && retour == null; i++)
            {
                retour = VisualTreeHelper.GetChild(element, i) as ScrollViewer;
                if (retour == null)
                {
                    retour = GetScrollViewer(VisualTreeHelper.GetChild(element, i) as UIElement);
                }
            }
            return retour;
        }

        private void DataGridTouchOptimized_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!wasMoved && SelectedItemChangedCommand != null && SelectedItemChangedCommand.CanExecute(null))
            {
                SelectedItemChangedCommand.Execute(SelectedItem);
            }
            wasMoved = false;
        }

        private void DataGridTouchOptimized_MouseMove(object sender, MouseEventArgs e)
        {
            var newMousePosition = Mouse.GetPosition((FrameworkElement)sender);

            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                wasMoved = true;

                if (newMousePosition.Y < oldMousePosition.Y)
                {
                    sv.ScrollToVerticalOffset(sv.VerticalOffset + 1);
                }
                if (newMousePosition.Y > oldMousePosition.Y)
                {
                    sv.ScrollToVerticalOffset(sv.VerticalOffset - 1);
                }
                if (newMousePosition.X < oldMousePosition.X)
                {
                    sv.ScrollToHorizontalOffset(sv.HorizontalOffset + 1);
                }
                if (newMousePosition.X > oldMousePosition.X)
                {
                    sv.ScrollToHorizontalOffset(sv.HorizontalOffset - 1);
                }
            }
            else
            {
                oldMousePosition = newMousePosition;
            }
        }

        private void DataGridTouchOptimized_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            sv = GetScrollViewer(this);
        }

        private void DataGridTouchOptimized_Unloaded(object sender, RoutedEventArgs e)
        {
            var grid = (System.Windows.Controls.DataGrid)sender;
            grid.CommitEdit(DataGridEditingUnit.Row, true);
        }
    }
}
