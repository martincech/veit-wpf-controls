﻿namespace Veit.Wpf.Controls.Models.Card
{
   public interface ICardLarge : ICard
   {
      string CurrentLabel { get; }
      string ScheduleLabel { get; }
      string FinishLabel { get; }

      int CurrentValue { get; }
      int ScheduleValue { get; }
      int FinishValue { get; }
   }
}
