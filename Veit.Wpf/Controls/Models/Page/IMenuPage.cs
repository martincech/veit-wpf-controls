﻿using System;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Controls.Models.Page
{
    public interface IMenuPage : INavigationPage
    {
        StockIcon Icon { get; }
        Uri BarMenu { get; }
    }
}
