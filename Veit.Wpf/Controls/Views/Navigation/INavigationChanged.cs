﻿namespace Veit.Wpf.Controls.Views.Navigation
{
    public interface INavigationChanged
    {
        void SelectionChanged(int index, bool isOptional);
    }
}
