﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Converters
{
   public class IconToVisibilityConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (value == null || !Enum.TryParse(value.ToString(), out StockIcon icon) || (int)icon == -1)
         {
            return Visibility.Collapsed;
         }
         return Visibility.Visible;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
