﻿using System;
using Veit.Wpf.Commands;
using Xunit;

namespace Veit.Wpf.Tests.Commands
{
    public class RelayCommandTests
    {
        private readonly RelayCommand<object> commandClassType;
        private readonly RelayCommand<DateTime> commandValueType;
        private readonly RelayCommand commandNonParam;

        private object classValue;
        private DateTime valueValue;
        private bool nonParamValue;

        public RelayCommandTests()
        {
            commandClassType = new RelayCommand<object>(
               d => classValue = d,
               d => d != null);
            commandValueType = new RelayCommand<DateTime>(
               d => valueValue = d,
               d => d != default(DateTime));
            commandNonParam = new RelayCommand(
               () => { },
               () => nonParamValue);

        }

        [Fact]
        public void CanExecuteIsFalse()
        {
            Assert.True(!commandClassType.CanExecute(null));
            Assert.True(!commandValueType.CanExecute(null));
            Assert.True(!commandNonParam.CanExecute(null));
        }

        [Fact]
        public void CanExecuteIsTrue()
        {
            Assert.True(commandClassType.CanExecute(new object()));
            Assert.True(commandValueType.CanExecute(DateTime.Now));
            nonParamValue = true;
            Assert.True(commandNonParam.CanExecute(null));
        }

        [Fact]
        public void Execute_Class_ThrowsException_WhenCanExecuteFalse()
        {
            Assert.Throws<InvalidOperationException>(() => commandClassType.Execute(null));
        }

        [Fact]
        public void ExecuteValue_ThrowsException_WhenCanExecuteFalse()
        {
            Assert.Throws<InvalidOperationException>(() => commandValueType.Execute(null));
        }

        [Fact]
        public void ExecuteNonParams_ThrowsException_WhenCanExecuteFalse()
        {
            Assert.Throws<InvalidOperationException>(() => commandNonParam.Execute(null));
        }

        [Fact]
        public void ExecuteExecuted()
        {
            var o = new object();
            commandClassType.Execute(o);
            Assert.Same(o, classValue);

            var d = DateTime.Now;
            commandValueType.Execute(d);
            Assert.Equal(d, valueValue);

            nonParamValue = true;
            commandNonParam.Execute(null);
            Assert.Equal(true, nonParamValue);
        }

        [Fact]
        public void CanExecuteChangedRaised()
        {
            var raised1 = false;
            commandClassType.CanExecuteChanged += (o, e) =>
            {
                raised1 = true;
            };
            var raised2 = false;
            commandValueType.CanExecuteChanged += (o, e) =>
            {
                raised2 = true;
            };
            var raised3 = false;
            commandNonParam.CanExecuteChanged += (o, e) =>
            {
                raised3 = true;
            };

            commandClassType.RaiseCanExecuteChanged();
            commandValueType.RaiseCanExecuteChanged();
            commandNonParam.RaiseCanExecuteChanged();
            Assert.True(raised1, "Can execute changed never raised");
            Assert.True(raised2, "Can execute changed never raised");
            Assert.True(raised3, "Can execute changed never raised");
        }
    }
}
