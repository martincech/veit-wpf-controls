﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Veit.Wpf.Converters
{
    /// <summary>
    /// Check if value is the same value as a parameter. Usage for radiobuttons for example.
    /// </summary>
    public class EnumToBoolConvertor : IValueConverter
    {
        // Converter accept parameter as Enum value or as string representation.
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return parameter is string ?
                value != null && value.ToString().Equals(parameter) :
                value != null && value.Equals(parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && value.Equals(true) ? parameter : Binding.DoNothing;
        }
    }
}
