﻿using Veit.Wpf.Controls.ViewModels.Wizard;

namespace Veit.Wpf.Controls.SampleViewModels
{
   public class SampleWizardViewModel : WizardViewModel
   {
      public SampleWizardViewModel()
         : base(null, null, null, "Some Title")
      {
      }
   }
}
