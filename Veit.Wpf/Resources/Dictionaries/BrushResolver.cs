﻿using MahApps.Metro;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Veit.Wpf.Resources.Dictionaries
{
    public static class BrushResolver
    {
        private static string selectedSource;
        private static ResourceDictionary RESOURCES;

        public static Dictionary<string, Uri> Sources { get; set; }

        public static string SelectedSource
        {
            get { return selectedSource; }
            set
            {
                selectedSource = value;
                SetResource();
            }
        }

        private static void SetResource()
        {
            if (Sources == null)
            {
                return;
            }
            if (Sources.TryGetValue(SelectedSource, out var uri))
            {
                RESOURCES = new ResourceDictionary
                {
                    Source = uri
                };

                var theme = ThemeManager.DetectAppStyle(Application.Current);
                if (theme == null)
                {
                    throw new InvalidOperationException("Cannot detect application style. Do you have add base style of MahApps.Metro in App.xaml?");
                }

                ThemeManager.ChangeAppStyle(Application.Current,
                                            ThemeManager.GetAccent(SelectedSource),
                                            theme.Item1);
            }
        }

        public static object GetBrushFromResources(Brush brush)
        {
            try
            {
                return RESOURCES[brush.ToString()];
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
