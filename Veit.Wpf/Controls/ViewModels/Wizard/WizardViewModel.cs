﻿using System;
using System.Windows.Input;
using Veit.Wpf.Commands;
using Veit.Wpf.Controls.Models.Page;

namespace Veit.Wpf.Controls.ViewModels.Wizard
{
   public abstract class WizardViewModel : PageViewModel
   {
      #region Private fields

      private ICommand previousCommand;
      private ICommand nextCommand;
      private ICommand closeCommand;
      private int step;
      private int stepCount;
      private string previousLabel;
      private string nextLabel;
      private object contentViewModel;
      private string title;

      private const string LABEL_CANCEL = "Cancel";
      private const string LABEL_CONTINUE = "Continue";
      private const string LABEL_BACK = "Back";
      private readonly string labelFinish;
      private readonly Action closeWizardAction;

      #endregion

      #region Public interfaces

      protected WizardViewModel(PageModel model, Action closeWizardAction, object contentVm, string title)
        : base(model)
      {
         labelFinish = title;
         this.closeWizardAction = closeWizardAction;
         ContentViewModel = contentVm;
         Title = labelFinish;
         Update();
      }

      public event EventHandler WizardFinished;
      public int Step { get { return step; } set { SetProperty(ref step, value); } }
      public int StepCount { get { return stepCount; } set { SetProperty(ref stepCount, value); } }
      public string PreviousLabel { get { return previousLabel; } set { SetProperty(ref previousLabel, value); } }
      public string NextLabel { get { return nextLabel; } set { SetProperty(ref nextLabel, value); } }
      public object ContentViewModel { get { return contentViewModel; } set { SetProperty(ref contentViewModel, value); } }
      public string Title { get { return title; } set { SetProperty(ref title, value); } }


      public ICommand PreviousCommand
      {
         get
         {
            return previousCommand ?? (previousCommand = new RelayCommand(
            () =>
            {
               if (Step > 0)
               {
                  Step--;
                  Update();
               }
               else if (Step == 0)
               {
                  closeWizardAction?.Invoke();
               }
            },
            () => true));
         }
      }

      public ICommand NextCommand
      {
         get
         {
            return nextCommand ?? (nextCommand = new RelayCommand(
            () =>
            {
               OnValidate();
               if (IsValidationError)
               {
                  return;
               }

               if (Step < StepCount - 1)
               {
                  Step++;
                  Update();
               }
               else if (Step == stepCount - 1)
               {
                  Invoke();
                  closeWizardAction?.Invoke();
               }
            },
            () => true));
         }
      }

      public ICommand CloseCommand
      {
         get
         {
            return closeCommand ?? (closeCommand = new RelayCommand(
            () =>
            {
               closeWizardAction?.Invoke();
            },
            () => true));
         }
      }

      #endregion

      private void Update()
      {
         PreviousLabel = Step == 0 ? LABEL_CANCEL : LABEL_BACK;
         NextLabel = Step == StepCount - 1 ? labelFinish : LABEL_CONTINUE;
      }

      private void Invoke()
      {
         WizardFinished?.Invoke(this, new EventArgs());
      }
   }
}
