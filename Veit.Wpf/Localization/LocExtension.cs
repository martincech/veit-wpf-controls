﻿using System.Windows.Data;

namespace Veit.Wpf.Localization
{
   public class LocExtension : Binding
   {
      public LocExtension(string name)
         : base("[" + name + "]")
      {
         Mode = BindingMode.OneWay;
         Source = TranslationSource.Instance;
      }
   }
}
