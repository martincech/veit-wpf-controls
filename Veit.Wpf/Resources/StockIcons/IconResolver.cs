﻿using System;
using System.Windows;

namespace Veit.Wpf.Resources.StockIcons
{
    public static class IconResolver
    {
        private static readonly ResourceDictionary RESOURCES = new ResourceDictionary
        {
            Source = new Uri("pack://application:,,,/Veit.Wpf;component/Resources/StockIcons/IcomoonConstants.xaml")
        };


        public static string GetTextFromIconFont(StockIcon flag)
        {
            return RESOURCES[flag.ToString()].ToString();
        }
    }
}
