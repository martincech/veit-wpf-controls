﻿using System.Windows.Controls;
using Veit.Wpf.Resources.Dictionaries;

namespace Veit.Wpf.Demo
{
    /// <summary>
    /// Interaction logic for DemoColorsWindow.xaml
    /// </summary>
    public partial class DemoColorsWindow
    {
        public DemoColorsWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!(sender is Button button)) return;

            var schema = "Colors";
            if (button.Content.ToString().Contains("Default"))
            {
                schema = "Colors";
            }
            else if (button.Content.ToString().Equals("Dark"))
            {
                schema = "DarkColors";
            }
            BrushResolver.SelectedSource = schema;
        }
    }
}
