﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Veit.Wpf.Controls.Models.Navigation;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Controls.ViewModels
{
    public class PageViewModel : ErrorValidationViewModel
    {
        private INavigationPage detailPage;
        private Uri barMenu;

        public PageViewModel(PageModel model)
        {
            if (model == null)
            {
                return;
            }

            MainMenuItems = model.MainMenuItems == null ? new ObservableCollection<IMenuPage>() : new ObservableCollection<IMenuPage>(model.MainMenuItems);
            MainMenuOptionItems = model.OptionalMenuItems == null ? new ObservableCollection<IMenuPage>() : new ObservableCollection<IMenuPage>(model.OptionalMenuItems);
            BarMenu = model.BarMenu;

            DefaultDetailPage = model.DefaultDetail;
            InitSelectedMenuIndex();

            DetailPage = new NavigationPage
            {
                Title = DefaultDetailPage.Title,
                Source = DefaultDetailPage.Source
            };
        }

        public class NavigationPage : IMenuPage
        {
            public string Title { get; set; }
            public Uri Source { get; set; }

            public StockIcon Icon => throw new NotImplementedException();

            public Uri BarMenu => throw new NotImplementedException();
        }

        public INavigationPage DefaultDetailPage { get; private set; }


        public int DefaultSelectedMenuIndex { get; private set; }

        public ObservableCollection<IMenuPage> MainMenuItems { get; private set; }
        public ObservableCollection<IMenuPage> MainMenuOptionItems { get; private set; }


        public Uri BarMenu { get { return barMenu; } set { SetProperty(ref barMenu, value); } }

        public INavigationPage DetailPage { get { return detailPage; } set { SetProperty(ref detailPage, value); } }

        public NavigationItem Map()
        {
            return new NavigationItem
            {
                MainMenuItems = MainMenuItems,
                OptionalMenuItems = MainMenuOptionItems,
                BarMenu = BarMenu,
                Item = DetailPage,
                SelectedMenuIndex = DefaultSelectedMenuIndex
            };
        }


        private void InitSelectedMenuIndex()
        {
            var defaultPage = MainMenuItems.FirstOrDefault(f => f.Source == DefaultDetailPage.Source);
            if (defaultPage == null)
            {
                defaultPage = MainMenuOptionItems.FirstOrDefault(f => f.Source == DefaultDetailPage.Source);
                DefaultSelectedMenuIndex = MainMenuOptionItems.IndexOf(defaultPage);
                if (DefaultSelectedMenuIndex >= 0)
                {
                    DefaultSelectedMenuIndex += MainMenuItems.Count();
                }
            }
            else
            {
                DefaultSelectedMenuIndex = MainMenuItems.IndexOf(defaultPage);
            }
        }
    }
}
