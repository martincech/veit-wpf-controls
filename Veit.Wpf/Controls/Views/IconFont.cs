﻿using System.Windows;
using System.Windows.Controls;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Controls.Views
{
    public class IconFont : Control
    {
        public static readonly DependencyProperty KindProperty = DependencyProperty.Register(nameof(Kind), typeof(StockIcon), typeof(IconFont), new PropertyMetadata(StockIcon.CheckMark));

        public StockIcon Kind
        {
            get { return (StockIcon)GetValue(KindProperty); }
            set { SetValue(KindProperty, value); }
        }


        static IconFont()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(IconFont), new FrameworkPropertyMetadata(typeof(IconFont)));
        }
    }
}
