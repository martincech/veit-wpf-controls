﻿using System;
using Veit.Wpf.Controls.Models;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Demo.NavigationPages
{
    public class Menu2Model : IMenuPage
    {
        public Menu2Model()
        {
            BarMenu = ModelHelper.GetUriFromType(typeof(Menu2BarMenuView));
            Source = ModelHelper.GetUriFromType(typeof(Menu2ContentView));
        }

        public bool IsOptional => false;
        public StockIcon Icon => StockIcon.DialMeter;
        public Uri BarMenu { get; set; }
        public string Title => "Menu 2";
        public Uri Source { get; set; }
    }
}
