﻿using System.Windows.Input;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Controls.Models.Card
{
    public interface ICard
    {
        string Title { get; }
        StockIcon Icon { get; }
        ICommand SwitchToDetailCommand { get; }
    }
}
