﻿using System;
using System.Collections.Generic;
using System.Linq;
using Veit.Wpf.Controls.Models.Navigation;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Resources.StockIcons;
using Xunit;

namespace Veit.Wpf.Tests.Controls.Models.Navigation
{
    public class NavigationItemTests
    {

        [Fact]
        public void EqualsWithoutMenuItems()
        {
            // Arrange
            var nav1 = GetDemoNavigationItem();
            var nav2 = GetDemoNavigationItem();

            // Act
            var result = nav1.Equals(nav2);

            // Assert
            Assert.True(result);
        }


        [Fact]
        public void NotEqualsMenuItemsDifferentCountOtherObjectIsNull()
        {
            // Arrange
            var nav1 = GetDemoNavigationItem();
            nav1.MainMenuItems = GetDemoMenuItems();
            var nav2 = GetDemoNavigationItem();

            // Act
            var result = nav1.Equals(nav2);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void NotEqualsMenuItemsDifferentCount()
        {
            // Arrange
            var nav1 = GetDemoNavigationItem();
            nav1.MainMenuItems = GetDemoMenuItems();
            var nav2 = GetDemoNavigationItem();
            var page = GetDemoMenuItems().First();
            nav2.MainMenuItems = new List<IMenuPage> { page };

            // Act
            var result = nav1.Equals(nav2);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void NotEqualsMenuItems()
        {
            // Arrange
            var nav1 = GetDemoNavigationItem();
            nav1.MainMenuItems = GetDemoMenuItems();
            var nav2 = GetDemoNavigationItem();
            nav2.MainMenuItems = GetDemoMenuItems();

            var page = nav2.MainMenuItems.ElementAt(0);
            page.Source = new Uri("http://necoqwertzzu.cz");

            // Act
            var result = nav1.Equals(nav2);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void EqualsWithMenuItems()
        {
            // Arrange
            var nav1 = GetDemoNavigationItem();
            nav1.MainMenuItems = GetDemoMenuItems();
            var nav2 = GetDemoNavigationItem();
            nav2.MainMenuItems = GetDemoMenuItems();

            // Act
            var result = nav1.Equals(nav2);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void NotEqualsMenuItemsDifferentOrder()
        {
            // Arrange
            var nav1 = GetDemoNavigationItem();
            nav1.MainMenuItems = GetDemoMenuItems();
            var nav2 = GetDemoNavigationItem();
            nav2.MainMenuItems = GetDemoMenuItems().Reverse();

            // Act
            var result = nav1.Equals(nav2);

            // Assert
            Assert.False(result);
        }


        #region Private helpers

        private static NavigationItem GetDemoNavigationItem()
        {
            return new NavigationItem
            {
                Item = new MockPage(),
                SelectedMenuIndex = -1
            };
        }

        private static IEnumerable<IMenuPage> GetDemoMenuItems()
        {
            var page1 = new MockPage
            {
                Title = "Page 1",
                Source = new Uri("https://localhost")
            };
            var page2 = new MockPage
            {
                Icon = StockIcon.Calendar,
                Title = "Page 2",
                Source = new Uri("http://localhost:1234")
            };

            return new List<IMenuPage>
            {
                page1,
                page2
            };
        }

        private class MockPage : IMenuPage
        {
            public StockIcon Icon { get; set; } = StockIcon.Add;

            public Uri BarMenu { get; set; }

            public string Title { get; set; } = "title";

            public Uri Source { get; set; }
        }

        #endregion
    }
}
