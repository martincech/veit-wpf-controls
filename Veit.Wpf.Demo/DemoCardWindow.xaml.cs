﻿using Veit.Wpf.Controls.ViewModels.Card;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Demo
{
    /// <summary>
    /// Interaction logic for DemoCardWindow.xaml
    /// </summary>
    public partial class DemoCardWindow
    {
        public DemoCardWindow()
        {
            InitializeComponent();
            Loaded += DemoCardWindow_Loaded;
        }

        private void DemoCardWindow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            smallCard.DataContext = new CardSmallViewModel(StockIcon.Bird, "My title", null)
            {
                Flag = CardState.Warning,
                Status = "status text",
            };
        }
    }
}
