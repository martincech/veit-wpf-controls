﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Demo
{
    /// <summary>
    /// Interaction logic for DemoIconsWindow.xaml
    /// </summary>
    public partial class DemoIconsWindow
    {
        public DemoIconsWindow()
        {
            InitializeComponent();
            Loaded += DemoIconsWindow_Loaded;
        }

        private void DemoIconsWindow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            table.ItemsSource = LoadAllIcons();
        }

        private static IEnumerable<DisplayIcon> LoadAllIcons()
        {
            var source = new List<DisplayIcon>();
            var enums = Enum.GetValues(typeof(StockIcon)).Cast<StockIcon>();

            foreach (var enumValue in enums.OrderBy(o => o.ToString()))
            {
                var value = IconResolver.GetTextFromIconFont(enumValue);
                var bytes = Encoding.Unicode.GetBytes(value).Reverse().ToArray();
                var hex = BitConverter.ToString(bytes).Replace("-", string.Empty);

                source.Add(new DisplayIcon
                {
                    Name = enumValue.ToString(),
                    Glyph = value,
                    HexCode = "x" + hex
                });
            }
            return source;
        }

        internal class DisplayIcon
        {
            public string Name { get; set; }
            public string Glyph { get; set; }
            public string HexCode { get; set; }
        }
    }
}
