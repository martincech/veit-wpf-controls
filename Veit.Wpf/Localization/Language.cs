﻿namespace Veit.Wpf.Localization
{
   public enum Language
   {
      English,
      Czech,
      French,
      German,
      Austrian
   }
}
