﻿using System;
using Veit.Wpf.Controls.Models.Card;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Controls.ViewModels.Card
{
   public class CardSmallViewModel : CardBaseViewModel, ICardSmall
   {
      private string status;
      private CardState flag;
      private StockIcon stateIcon;

      public CardSmallViewModel(StockIcon icon, string title, Action switchToDetailAction)
         : base(icon, title, switchToDetailAction)
      {
         StateIcon = StockIcon.CheckMark;
         Flag = CardState.Ok;
      }

      public string Status { get { return status; } set { SetProperty(ref status, value); } }
      public CardState Flag { get { return flag; } set { SetProperty(ref flag, value); } }
      public StockIcon StateIcon { get { return stateIcon; } set { SetProperty(ref stateIcon, value); } }
   }
}
