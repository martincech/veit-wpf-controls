﻿namespace Veit.Wpf.Controls.Models
{
   public interface IErrorView
   {
      void DisplayError(string message);
   }
}
