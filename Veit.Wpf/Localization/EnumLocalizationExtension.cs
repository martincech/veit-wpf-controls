﻿using System;

namespace Veit.Wpf.Localization
{
   public static class EnumLocalizationExtension
   {
      public static string GetLocalizableName(this Enum e)
      {
         return TranslationSource.Instance[e.ToString()];
      }
   }
}
