﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Veit.Wpf.Controls.ViewModels.Calendar;
using Veit.Wpf.Resources.Dictionaries;

namespace Veit.Wpf.Converters
{
   public class EventSummaryForegroundConverter : IValueConverter
   {
      private const Resources.Dictionaries.Brush DARK = Resources.Dictionaries.Brush.Dark;
      private const Resources.Dictionaries.Brush LIGHT = Resources.Dictionaries.Brush.White;

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (value == null || !(value is TaskEventViewModel vm))
         {
            return new SolidColorBrush(Colors.Transparent);
         }

         var currentDate = DateTime.Now;
         var timeStampCompareResult = DateTime.Compare(vm.TimeStamp.Date, currentDate.Date);
         var brush = LIGHT;
         if (timeStampCompareResult < 0)
         {  //passed
            brush = DARK;
         }
         else if (timeStampCompareResult == 0)  //current
         {
            if (vm.StartHour == 0 && vm.EndHour == 0)
            {
               brush = LIGHT;
            }
            else
            {  //partial day events
               if (currentDate.Hour > vm.EndHour)
               {  //passed
                  brush = DARK;
               }
            }
         }

         return BrushResolver.GetBrushFromResources(brush);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
