﻿using System;

namespace Veit.Wpf.Controls.Models.Page
{
    public static class PageExtension
    {
        public static bool PageIsSame(this INavigationPage p1, INavigationPage p2)
        {
            if (p1 == null && p2 == null)
            {
                return true;
            }
            if (p1 == null || p2 == null)
            {
                return false;
            }

            return
                (
                    p1.Source == p2.Source ||
                    p1.Source != null &&
                    p1.Source.Equals(p2.Source)
                ) &&
                (
                    p1.Title == p2.Title ||
                    p1.Title != null &&
                    p1.Title.Equals(p2.Title, StringComparison.Ordinal)
                );
        }
    }
}
