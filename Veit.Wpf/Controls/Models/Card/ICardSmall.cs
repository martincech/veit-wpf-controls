﻿namespace Veit.Wpf.Controls.Models.Card
{
   public interface ICardSmall : ICard
   {
      string Status { get; }
   }
}
