﻿using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Veit.Wpf.Controls.Views.Navigation
{
    public class NavigationView : HamburgerMenu
    {
        #region Dependency properties

        private const string DEFAULT_BACK_LABEL = "BACK";
        private const string DEFAULT_MENU_LABEL = "MENU";

        /// <summary>
        /// Identifies the <see cref="BackWidth"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty BackWidthProperty = DependencyProperty.Register(nameof(BackWidth), typeof(double), typeof(NavigationView), new PropertyMetadata(48.0));

        /// <summary>
        /// Identifies the <see cref="BackHeight"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty BackHeightProperty = DependencyProperty.Register(nameof(BackHeight), typeof(double), typeof(NavigationView), new PropertyMetadata(48.0));

        /// <summary>
        /// Identifies the <see cref="BackVisibility"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty BackVisibilityProperty = DependencyProperty.Register(nameof(BackVisibility), typeof(Visibility), typeof(NavigationView), new PropertyMetadata(Visibility.Visible));

        /// <summary>
        /// Identifies the <see cref="BackCommand"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty BackCommandProperty = DependencyProperty.Register(nameof(BackCommand), typeof(ICommand), typeof(NavigationView), new PropertyMetadata(null));

        /// <summary>
        /// Identifies the <see cref="BackLabel"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty BackLabelProperty = DependencyProperty.Register(nameof(BackLabel), typeof(string), typeof(NavigationView), new PropertyMetadata(DEFAULT_BACK_LABEL));

        /// <summary>
        /// Identifies the <see cref="MenuLabel"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty MenuLabelProperty = DependencyProperty.Register(nameof(MenuLabel), typeof(string), typeof(NavigationView), new PropertyMetadata(DEFAULT_MENU_LABEL));

        #endregion

        #region Back button properties

        /// <summary>
        /// Gets or sets main button's width.
        /// </summary>
        public double BackWidth
        {
            get { return (double)GetValue(BackWidthProperty); }
            set { SetValue(BackWidthProperty, value); }
        }

        /// <summary>
        /// Gets or sets main button's height.
        /// </summary>
        public double BackHeight
        {
            get { return (double)GetValue(BackHeightProperty); }
            set { SetValue(BackHeightProperty, value); }
        }


        /// <summary>
        /// Gets or sets back button's visibility.
        /// </summary>
        public Visibility BackVisibility
        {
            get { return (Visibility)GetValue(BackVisibilityProperty); }
            set { SetValue(BackVisibilityProperty, value); }
        }

        /// <summary>
        /// Gets or sets back button's command.
        /// </summary>
        public ICommand BackCommand
        {
            get { return (ICommand)GetValue(BackCommandProperty); }
            set { SetValue(BackCommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets back button's label.
        /// </summary>
        public string BackLabel
        {
            get { return (string)GetValue(BackLabelProperty); }
            set { SetValue(BackLabelProperty, value); }
        }

        #endregion

        #region Menu button properties

        /// <summary>
        /// Gets or sets hamburger menu button's label.
        /// </summary>
        public string MenuLabel
        {
            get { return (string)GetValue(MenuLabelProperty); }
            set { SetValue(MenuLabelProperty, value); }
        }

        #endregion

        #region Constructors

        static NavigationView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NavigationView), new FrameworkPropertyMetadata(typeof(NavigationView)));
        }

        public NavigationView()
        {
            ItemInvoked += NavigationView_ItemInvoked;
        }

        #endregion

        public override void OnApplyTemplate()
        {
            var backButton = (Button)GetTemplateChild("HamburgerBackButton");
            backButton.Click += BackButton_Click;

            base.OnApplyTemplate();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            //Hide pane panel when click on back button
            IsPaneOpen = false;
        }

        private void NavigationView_ItemInvoked(object sender, HamburgerMenuItemInvokedEventArgs e)
        {
            Content = e.InvokedItem;
            IsPaneOpen = false;

            if (DataContext is INavigationChanged navigator)
            {
                var index = SelectedIndex;
                var isOptional = false;
                if (e.IsItemOptions)
                {
                    isOptional = true;
                    index = SelectedOptionsIndex;
                }
                navigator.SelectionChanged(index, isOptional);
            }
        }
    }
}
