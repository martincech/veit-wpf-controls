﻿using System;
using System.Windows;
using Veit.Wpf.Controls.ViewModels.Wizard;

namespace Veit.Wpf.Demo
{
    /// <summary>
    /// Interaction logic for DemoWizardWindow.xaml
    /// </summary>
    public partial class DemoWizardWindow
    {
        public DemoWizardWindow()
        {
            InitializeComponent();
            Loaded += DemoWizardWindow_Loaded;
        }

        private void DemoWizardWindow_Loaded(object sender, RoutedEventArgs e)
        {
            wizard.DataContext = new DemoWizardVm(() => { Close(); });
        }


        internal class DemoWizardVm : WizardViewModel
        {
            public DemoWizardVm(Action closeAction)
                : base(null, closeAction, null, "Demo wizard")
            {
                WizardFinished += DemoWizardVm_WizardFinished;
            }

            private static void DemoWizardVm_WizardFinished(object sender, EventArgs e)
            {
                MessageBox.Show("Wizard was successfully finished");
            }
        }
    }
}
