﻿using Veit.Wpf.Mvvm.Observable;
using System.Collections.ObjectModel;

namespace Veit.Wpf.Controls.ViewModels.Calendar
{
   public class EventSummaryViewModel : ObservableObject
   {
      private ObservableCollection<TaskEventViewModel> events;



      public ObservableCollection<TaskEventViewModel> Events { get { return events; } set { SetProperty(ref events, value); } }
   }
}
