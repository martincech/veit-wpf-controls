﻿using MahApps.Metro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Veit.Wpf.Resources.Dictionaries;

namespace Veit.Wpf
{
    public static class Setup
    {
        private const string DEFAULT_COLOR_SCHEMA = nameof(Colors);
        private const string DARK_COLOR_SCHEMA = "DarkColors";


        /// <summary>
        /// Load default fonts and init default theme.
        /// </summary>
        public static void InitResources()
        {
            InitTheme();
            CheckFonts();
        }


        private static void InitTheme()
        {
            BrushResolver.Sources = new Dictionary<string, Uri>
             {
                { DEFAULT_COLOR_SCHEMA,  new Uri("pack://application:,,,/Veit.Wpf;component/Resources/Dictionaries/Colors.xaml") },
                { DARK_COLOR_SCHEMA,  new Uri("pack://application:,,,/Veit.Wpf;component/Resources/Dictionaries/DarkColors.xaml") }
                //add next accent resources

             };
            foreach (var source in BrushResolver.Sources)
            {
                ThemeManager.AddAccent(source.Key, source.Value);
            }

            //when you want to change color resource, change only SelectedSource
            BrushResolver.SelectedSource = DEFAULT_COLOR_SCHEMA;
        }



        #region Check fonts

        private const string expectedFontName = "Segoe UI";

        private static void CheckFonts()
        {
            CheckFont("FontRegular", "AlternativeFontRegular");
            CheckFont("FontSemibold", "AlternativeFontBold");
            CheckFont("FontLight", "AlternativeFontLight");
        }

        private static void CheckFont(string fontResourceKey, string alternativeFontresourceKey)
        {
            if (!FontFound(fontResourceKey))
            {
                Application.Current.Resources[fontResourceKey] = Application.Current.Resources[alternativeFontresourceKey];
            }
        }

        private static bool FontFound(string resourceKey)
        {
            if (!(Application.Current.Resources[resourceKey] is FontFamily font))
            {
                return false;
            }
            if (font.FamilyNames.Any())
            {
                var name = font.FamilyNames.Values.ElementAt(0);
                if (name.Contains(expectedFontName))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
