﻿using Veit.Wpf.Mvvm.Observable;
using Xunit;

namespace MvvmFramework.Tests.UnitTests
{
   public class ObservableObjectTests
   {
      [Fact]
      public void PropertyChangedEventHandlerIsRaised_WhenCalledByName()
      {
         var obj = new StubObservableObject();
         var raised = false;

         obj.PropertyChanged += (sender, e) =>
         {
            Assert.True(e.PropertyName.Equals("ChangedProperty"));
            raised = true;
         };

         obj.ChangedProperty = "Some Value";

         Assert.True(raised, "PropertyChanged never invoked");
      }

      [Fact]
      public void PropertyChangedEventHandlerIsRaised_WhenCalledByExpression()
      {
         var obj = new StubObservableObject();
         var raised = false;

         obj.PropertyChanged += (sender, e) =>
         {
            Assert.True(e.PropertyName.Equals("ChangedPropertyExpr"));
            raised = true;
         };

         obj.ChangedPropertyExpr = "Some Value";

         Assert.True(raised, "PropertyChanged never invoked");
      }
   }

   internal class StubObservableObject : ObservableObject
   {
      private string changedProperty;
      private string changedPropertyExpr;

      public string ChangedProperty
      {
         get { return changedProperty; }
         set { SetProperty(ref changedProperty, value); }
      }

      public string ChangedPropertyExpr
      {
         get { return changedPropertyExpr; }
         set
         {
            if (value == changedPropertyExpr) return;
            changedPropertyExpr = value;
            RaisePropertyChanged(() => ChangedPropertyExpr);
         }
      }
   }
}
