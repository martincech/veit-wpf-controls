﻿using Veit.Wpf.Controls.ViewModels;

namespace Veit.Wpf.Controls.SampleViewModels
{
   public class SampleErrorValidationViewModel : ErrorValidationViewModel
   {
      public SampleErrorValidationViewModel()
      {
         ValidationErrorText = "Test Error Description ...";
      }
   }
}
