﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Data;

namespace Veit.Wpf.Converters
{
   public class HexStringToIntConverter : IValueConverter
   {
      private string lastValidValue;
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (value == null || !(value is int)) return null;
         return ((int) value).ToString("X");
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (value == null || !(value is string)) return null;
         var valueAsString = ((string)value).Replace(" ", string.Empty).ToUpper();
         lastValidValue = IsHex(valueAsString) ? valueAsString : lastValidValue;
         return System.Convert.ToInt32(lastValidValue, 16);
      }


      private static bool IsHex(string text)
      {
         return Regex.IsMatch(text, "^[0-9A-Fa-f]*$");
      }
   }
}
