﻿using System.Windows;
using System.Windows.Controls;

namespace Veit.Wpf.Controls.Views.Card
{
    public class Card : Button
    {
        static Card()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Card), new FrameworkPropertyMetadata(typeof(Card)));
        }
    }
}
