﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Veit.Wpf.Converters
{
   public class UpdateIndexConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var index = 0;
         if (value == null || !int.TryParse(value.ToString(), out index))
         {
            return index;
         }
         return index + 1;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
