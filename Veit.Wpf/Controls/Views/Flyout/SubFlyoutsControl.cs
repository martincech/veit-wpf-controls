﻿using MahApps.Metro.Controls;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Veit.Wpf.Commands;

namespace Veit.Wpf.Controls.Views.Flyout
{
    /// <summary>
    /// FlyoutsControl that can be used in UserControls.
    /// Its flyouts controls are injected into windows existed flyouts.
    /// </summary>
    public class SubFlyoutsControl : FlyoutsControl
    {
        #region Private fields

        private MetroWindow window;
        private int controlsCount;

        #endregion


        public static readonly DependencyProperty ShowFlyoutCommandProperty = DependencyProperty.Register(nameof(ShowFlyoutCommand), typeof(RelayCommand<string>), typeof(SubFlyoutsControl), new PropertyMetadata(null));

        public ICommand ShowFlyoutCommand
        {
            get { return (ICommand)GetValue(ShowFlyoutCommandProperty); }
            set { SetValue(ShowFlyoutCommandProperty, value); }
        }

        static SubFlyoutsControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SubFlyoutsControl), new FrameworkPropertyMetadata(typeof(SubFlyoutsControl)));
        }

        public SubFlyoutsControl()
        {
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                return;
            }

            Loaded += SubFlyoutsControl_Loaded;
            Unloaded += SubFlyoutsControl_Unloaded;

            ShowFlyoutCommand = new RelayCommand<string>(
               (o) =>
               {
                   if (o == null || !int.TryParse(o, out var param))
                   {
                       param = 0;
                   }
                   ToggleFlyout(param);
               });
        }

        #region Private helpers

        private void SubFlyoutsControl_Loaded(object sender, RoutedEventArgs e)
        {
            GetWindowInstance();
            controlsCount = Items.Count;
            CleanUpWindowFlyouts();

            var controls = new List<MahApps.Metro.Controls.Flyout>();

            //before adding controls to window flyouts they must be removed from current control treeview
            foreach (var item in Items)
            {
                if (!(item is MahApps.Metro.Controls.Flyout flyout)) continue;
                controls.Add(flyout);
            }
            Items.Clear();

            foreach (var item in controls)
            {
                window.Flyouts.Items.Add(item);
            }
        }

        private void GetWindowInstance()
        {
            window = Window.GetWindow(this) as MetroWindow;
            if (window == null)
            {
                throw new System.InvalidCastException("Control's parent window is not type of MetroWindow!");
            }
        }

        private void SubFlyoutsControl_Unloaded(object sender, RoutedEventArgs e)
        {
            foreach (var item in window.Flyouts.Items)
            {
                if (!(item is MahApps.Metro.Controls.Flyout flyout)) continue;
                flyout.IsOpen = false;
            }
            CleanUpWindowFlyouts();
        }

        private void ToggleFlyout(int index)
        {
            var globalCount = window.Flyouts.Items.Count;
            index = index + (globalCount - controlsCount);

            if (index + 1 > globalCount || !(window.Flyouts.Items[index] is MahApps.Metro.Controls.Flyout flyoutView))
            {
                return;
            }
            flyoutView.IsOpen = !flyoutView.IsOpen;
        }

        /// <summary>
        /// Remove local flyouts from window control.
        /// </summary>
        private void CleanUpWindowFlyouts()
        {
            var globalCount = window.Flyouts.Items.Count;
            for (var i = globalCount - 1; i > globalCount - controlsCount; i--)
            {
                window.Flyouts.Items.RemoveAt(i);
            }
        }

        #endregion
    }
}
