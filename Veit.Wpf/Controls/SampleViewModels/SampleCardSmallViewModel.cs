﻿using Veit.Wpf.Controls.ViewModels.Card;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Controls.SampleViewModels
{
   public class SampleCardSmallViewModel : CardSmallViewModel
   {
      public SampleCardSmallViewModel()
         : base(StockIcon.CloudUpload, "Cloud", null)
      {
         Status = "CONNECTED";
         Flag = CardState.Ok;
         StateIcon = StockIcon.CheckMark;
      }
   }
}
