﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Veit.Wpf.Converters
{
   public class EventInfoItemVisibilityConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         const Visibility visibility = Visibility.Collapsed;
         if (value == null ||
             !int.TryParse(value.ToString(), out var collectionCount) ||
             !int.TryParse(parameter.ToString(), out var order))
         {
            return visibility;
         }

         if (order >= collectionCount)
         {
            return visibility;
         }
         return Visibility.Visible;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
