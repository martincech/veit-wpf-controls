﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Veit.Wpf.Commands;

namespace Veit.Wpf.Controls.Views.DataGrid
{
    public class DataGridPaging : System.Windows.Controls.DataGrid
    {
        private IEnumerable<object> innerPages;
        private const int DEFAULT_PAGE_SIZE = 5;

        #region Dependency properties

        public static readonly DependencyProperty CurrentPageProperty = DependencyProperty.Register(nameof(CurrentPage), typeof(int), typeof(DataGridPaging), new PropertyMetadata(0));
        public static readonly DependencyProperty PageCountProperty = DependencyProperty.Register(nameof(PageCount), typeof(int), typeof(DataGridPaging), new PropertyMetadata(0));
        public static readonly DependencyProperty PagesProperty = DependencyProperty.Register(nameof(Pages), typeof(IEnumerable), typeof(DataGridPaging), new PropertyMetadata(null, PropertyChanged));
        public static readonly DependencyProperty PageSizeProperty = DependencyProperty.Register(nameof(PageSize), typeof(int), typeof(DataGridPaging), new PropertyMetadata(DEFAULT_PAGE_SIZE));
        public static readonly DependencyProperty CanPreviousProperty = DependencyProperty.Register(nameof(CanPrevious), typeof(bool), typeof(DataGridPaging), new PropertyMetadata(false));
        public static readonly DependencyProperty CanNextProperty = DependencyProperty.Register(nameof(CanNext), typeof(bool), typeof(DataGridPaging), new PropertyMetadata(false));

        public int CurrentPage
        {
            get { return (int)GetValue(CurrentPageProperty); }
            private set { SetValue(CurrentPageProperty, value); }
        }

        public int PageCount
        {
            get { return (int)GetValue(PageCountProperty); }
            private set { SetValue(PageCountProperty, value); }
        }

        public IEnumerable Pages
        {
            get { return (IEnumerable)GetValue(PagesProperty); }
            set { SetValue(PagesProperty, value); }
        }

        public int PageSize
        {
            get { return (int)GetValue(PageSizeProperty); }
            set
            {
                SetValue(PageSizeProperty, value);
                UpdatePaging();
            }
        }

        public bool CanPrevious
        {
            get { return (bool)GetValue(CanPreviousProperty); }
            private set { SetValue(CanPreviousProperty, value); }
        }

        public bool CanNext
        {
            get { return (bool)GetValue(CanNextProperty); }
            private set { SetValue(CanNextProperty, value); }
        }

        #endregion

        static DataGridPaging()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DataGridPaging), new FrameworkPropertyMetadata(typeof(DataGridPaging)));
        }

        public DataGridPaging()
        {
            Loaded += DataGridPaging_Loaded;
        }

        private void DataGridPaging_Loaded(object sender, RoutedEventArgs e)
        {
            SelectedItem = null;
        }



        private static void PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property.Name.Equals(nameof(Pages)))
            {
                if (!(obj is DataGridPaging control)) return;
                if (control.Pages == null) return;
                control.innerPages = control.Pages.Cast<object>();
                control.UpdatePaging();
            }
        }

        public ICommand PreviousPageCommand
        {
            get
            {
                return new RelayCommand(
                () =>
                {
                    ChangePage(CurrentPage - 1);
                },
                () => true);
            }
        }

        public ICommand NextPageCommand
        {
            get
            {
                return new RelayCommand(
                () =>
                {
                    ChangePage(CurrentPage + 1);
                },
                () => true);
            }
        }

        private void ChangePage(int newPage)
        {
            if (newPage < 1)
            {
                newPage = 1;
            }
            if (newPage > PageCount)
            {
                newPage = PageCount;
            }

            ItemsSource = innerPages.Page(newPage, PageSize);
            CurrentPage = newPage;
            UpdateCommands();
        }

        private void UpdatePaging()
        {
            if (Pages == null)
            {
                PageCount = 0;
                return;
            }

            var count = innerPages.Count();
#pragma warning disable CC0014 // Use ternary operator
            if (count == 0)
            {
                PageCount = 0;
            }
            else if (count < PageSize)
            {
                PageCount = 1;
            }
            else
            {
                PageCount = (int)Math.Ceiling(count / (double)PageSize);
            }
#pragma warning restore CC0014 // Use ternary operator
            ChangePage(1);
        }

        private void UpdateCommands()
        {
            CanPrevious = CurrentPage > 1;
            CanNext = CurrentPage < PageCount;

            ((RelayCommand)PreviousPageCommand).RaiseCanExecuteChanged();
            ((RelayCommand)NextPageCommand).RaiseCanExecuteChanged();
        }
    }
}
