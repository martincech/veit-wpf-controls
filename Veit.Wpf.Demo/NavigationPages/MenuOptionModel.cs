﻿using System;
using Veit.Wpf.Controls.Models;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Demo.NavigationPages
{
    public class MenuOptionModel : IMenuPage
    {
        public MenuOptionModel()
        {
            BarMenu = ModelHelper.GetUriFromType(typeof(MenuOptionBarMenuView));
            Source = ModelHelper.GetUriFromType(typeof(MenuOptionContentView));
        }

        public bool IsOptional => true;
        public StockIcon Icon => StockIcon.Feedback;
        public Uri BarMenu { get; set; }
        public string Title => "Option";
        public Uri Source { get; set; }
    }
}
