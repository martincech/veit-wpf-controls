﻿using System.Windows;
using System.Windows.Controls;

namespace Veit.Wpf.Controls.Views.Calendar
{
   /// <summary>
   /// Interaction logic for EventSummaryView.xaml
   /// </summary>
   public partial class EventSummaryView : UserControl
   {
      private static CornerRadius defaultCornerRadius = new CornerRadius(5, 5, 5, 5);
      private static Thickness defaultItemPadding = new Thickness(0);

      public EventSummaryView()
      {
         InitializeComponent();
      }

      public static readonly DependencyProperty ItemCornerRadiusProperty = DependencyProperty.Register(nameof(ItemCornerRadius), typeof(CornerRadius), typeof(EventSummaryView), new PropertyMetadata(defaultCornerRadius));
      public static readonly DependencyProperty ItemPaddingProperty = DependencyProperty.Register(nameof(ItemPadding), typeof(Thickness), typeof(EventSummaryView), new PropertyMetadata(defaultItemPadding));

      public CornerRadius ItemCornerRadius
      {
         get { return (CornerRadius)GetValue(ItemCornerRadiusProperty); }
         set { SetValue(ItemCornerRadiusProperty, value); }
      }

      public Thickness ItemPadding
      {
         get { return (Thickness)GetValue(ItemPaddingProperty); }
         set { SetValue(ItemPaddingProperty, value); }
      }
   }
}
