﻿using System;
using System.Windows;
using Veit.Wpf;

namespace Veit.Wpf.Demo
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
   {
        protected override void OnStartup(StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            base.OnStartup(e);
        }

        private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            var e = (Exception)args.ExceptionObject;
            MessageBox.Show(e.Message + "\n\nTrace:\n" + e.StackTrace);
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Setup.InitResources();

            MainWindow = new MainWindow();
            MainWindow.Show();
        }
    }
}
