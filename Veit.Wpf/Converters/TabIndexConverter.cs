﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace Veit.Wpf.Converters
{
    public class TabIndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var msg = "";
            if (parameter != null)
            {
                msg = parameter + " ";
            }

            if (!(value is TabItem tabItem))
            {
                return null;
            }

            var index = ItemsControl.ItemsControlFromItemContainer(tabItem)
             .ItemContainerGenerator.IndexFromContainer(tabItem) + 1;

            return msg + index;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
