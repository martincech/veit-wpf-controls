﻿using System;
using System.Collections.Generic;
using System.Linq;
using Veit.Wpf.Controls.Models.Page;

namespace Veit.Wpf.Controls.Models.Navigation
{
    public class NavigationService : INavigationService
    {
        private readonly object lockObj = new object();
        private readonly List<NavigationItem> historic;

        public NavigationService()
        {
            historic = new List<NavigationItem>();
        }

        public event EventHandler<NavigationItem> CurrentPageChanged;

        public NavigationItem CurrentPage { get; private set; }
        public NavigationItem HomePage { get; private set; }

        public object Parameter { get; private set; }

        public bool CanGoBack { get { return historic.Count > 1; } }

        public void GoBack()
        {
            lock(lockObj)
            {
                if (CanGoBack)
                {
                    historic.RemoveAt(historic.Count - 1);
                    CurrentPage = historic.Last();
                    OnCurrentPageChange();
                }
            }
        }

        public void NavigateTo(NavigationItem page)
        {
            lock(lockObj)
            {
                TryClearHistory(page.Item);
                NavigateRoutine(page, null);
            }
        }

        public void NavigateTo(NavigationItem page, object parameter)
        {
            lock(lockObj)
            {
                NavigateRoutine(page, parameter);
            }
        }

        private void NavigateRoutine(NavigationItem page, object parameter)
        {
            if (CurrentPage != null && CurrentPage.Equals(page))
            {
                return;
            }
            if (!historic.Any())
            {   //when you call navigate routine at the first time, home page is initialize.
                HomePage = page;
                historic.Add(page);
            }

            if (!IsHomePage(page.Item))
            {   //Do not add home page again to history stack
                historic.Add(page);
            }

            Parameter = parameter;
            CurrentPage = page;
            OnCurrentPageChange();
        }


        private void OnCurrentPageChange()
        {
            CurrentPageChanged?.Invoke(this, CurrentPage);
        }

        /// <summary>
        /// Delete whole history of visited pages if new page is home page.
        /// </summary>
        /// <param name="page"></param>
        private void TryClearHistory(INavigationPage page)
        {
            var removedCount = historic.Count - 1;
            if (IsHomePage(page) && removedCount > 0)
            {
                historic.RemoveRange(1, removedCount);
            }
        }

        private bool IsHomePage(INavigationPage page)
        {
            if (page == null || HomePage == null) return false;
            return HomePage.Item.Source.ToString().Equals(page.Source.ToString());
        }
    }
}
