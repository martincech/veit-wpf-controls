﻿using System;
using System.Collections.ObjectModel;

namespace Veit.Wpf.Controls.ViewModels.Calendar
{
   public interface ITaskViewModel
   {
      DateTime DisplayMonth { get; }
      ObservableCollection<TaskEventViewModel> SelectedMonthEvents { get; }


      ObservableCollection<TaskEventViewModel> GetMonthlyEvents(DateTime timeStamp);
   }
}
