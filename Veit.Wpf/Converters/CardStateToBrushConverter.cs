﻿using System;
using System.Globalization;
using System.Windows.Data;
using Veit.Wpf.Controls.ViewModels.Card;
using Veit.Wpf.Resources.Dictionaries;

namespace Veit.Wpf.Converters
{
    public class CardStateToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var selectBrush = Brush.Dark;
            if (value == null || !Enum.TryParse(value.ToString(), out CardState flag))
            {
                return BrushResolver.GetBrushFromResources(selectBrush);
            }

            switch (flag)
            {
                case CardState.Ok:
                    selectBrush = Brush.AccentBrush;
                    break;
                case CardState.Error:
                    selectBrush = Brush.Error;
                    break;
                case CardState.Warning:
                    selectBrush = Brush.Warning;
                    break;
                default:
                    selectBrush = Brush.Dark;
                    break;
            }
            return BrushResolver.GetBrushFromResources(selectBrush);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
