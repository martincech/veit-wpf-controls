﻿using Veit.Wpf.Demo.Data;

namespace Veit.Wpf.Demo
{
    /// <summary>
    /// Interaction logic for DemoDataGridPagingWindow.xaml
    /// </summary>
    public partial class DemoDataGridPagingWindow
    {
        public DemoDataGridPagingWindow()
        {
            InitializeComponent();
            dataGrid.Pages = new DataGridSource().Source;
        }
    }
}
