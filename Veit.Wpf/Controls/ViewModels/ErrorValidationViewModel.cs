﻿using Veit.Wpf.Mvvm.Observable;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Veit.Wpf.Controls.ViewModels
{
   public abstract class ErrorValidationViewModel : ObservableObject
   {
      private bool isValidationError;
      private string validationErrorText;

      private const string DEFAULT_ERROR = "Unknown error";


      public bool IsValidationError { get { return isValidationError; } set { SetProperty(ref isValidationError, value); } }
      public string ValidationErrorText { get { return validationErrorText; } set { SetProperty(ref validationErrorText, value); } }


      protected void SetError(string message)
      {
         ValidationErrorText = string.IsNullOrEmpty(message) ? DEFAULT_ERROR : message;
         IsValidationError = true;
      }

      protected void SetError(IEnumerable<string> messages)
      {
         if (messages == null || !messages.Any())
         {
            ValidationErrorText = DEFAULT_ERROR;
         }
         else
         {
            ValidationErrorText = "";
            var count = messages.Count();
            var builder = new StringBuilder();
            builder.Append(ValidationErrorText);
            for (int i = 0; i < count; i++)
            {
               builder.Append(messages.ElementAt(i));
               if (i + 1 < count)
               {
                  builder.Append("\n");
               }
            }
            ValidationErrorText = builder.ToString();
         }

         IsValidationError = true;
      }



      protected virtual void OnValidate()
      {
         ValidationErrorText = DEFAULT_ERROR;
         IsValidationError = false;
      }
   }
}
