﻿namespace Veit.Wpf.Controls.ViewModels.Card
{
   public enum CardState
   {
      Ok,
      Neutral,
      Error,
      Warning
   }
}
