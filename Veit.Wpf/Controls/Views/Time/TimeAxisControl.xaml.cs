﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using Veit.Wpf.Controls.ViewModels.Calendar;

namespace Veit.Wpf.Controls.Views.Time
{
   /// <summary>
   /// Interaction logic for TimeAxisControl.xaml
   /// </summary>
   public partial class TimeAxisControl : UserControl
   {
      private const int MAXIMUM_ROW_INDEX = 7;

      public TimeAxisControl()
      {
         InitializeComponent();
         DataContextChanged += TimeAxisControl_DataContextChanged;
      }

      public static readonly DependencyProperty StartTimeHourProperty = DependencyProperty.Register(nameof(StartTimeHour), typeof(int), typeof(TimeAxisControl), new PropertyMetadata(0, PropertyChanged));

      public int StartTimeHour
      {
         get { return (int)GetValue(StartTimeHourProperty); }
         set { SetValue(StartTimeHourProperty, value); }
      }


      #region Private helpers

      private void TimeAxisControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
      {
         contentGrid.Children.Clear();
         AddDefaultControls();

         if (!(DataContext is ObservableCollection<TaskEventViewModel> data))
         {
            return;
         }

         var start = StartTimeHour;
         var end = start + MAXIMUM_ROW_INDEX;

         foreach (var item in data)
         {
            //starting event in this time window
            if (item.StartHour >= start && item.StartHour <= end) //event below this time window
            {
               //get start row index
               var startRowIndex = item.StartHour - start + 1;

               var endRowIndex = item.EndHour - start;
               if (item.EndHour > end)
               {
                  endRowIndex = MAXIMUM_ROW_INDEX + 1;
               }

               var view = new TimeRecordView
               {
                  DataContext = item,
                  Margin = new Thickness(0, -2, 0, 0)
               };

               contentGrid.Children.Add(view);

               Grid.SetColumn(view, 2);
               Grid.SetRow(view, startRowIndex);
               Grid.SetRowSpan(view, endRowIndex - startRowIndex + 1);
            }


            //event that is continue from previous time window
            if (item.StartHour < start && item.EndHour >= start)
            {
               var view = new TimeRecordView
               {
                  DataContext = new TaskEventViewModel
                  {
                     Title = ". . .",
                     StartHour = item.StartHour,
                     EndHour = item.EndHour,
                     Type = item.Type,
                     TimeStamp = item.TimeStamp
                  }
               };

               contentGrid.Children.Add(view);

               Grid.SetColumn(view, 2);
               Grid.SetRow(view, 0);

               var rowSpan = item.EndHour - start + 1;
               Grid.SetRowSpan(view, rowSpan);
            }

         }
      }

      private void AddDefaultControls()
      {
         contentGrid.Children.Add(text0);
         contentGrid.Children.Add(text1);
         contentGrid.Children.Add(text2);
         contentGrid.Children.Add(text3);
         contentGrid.Children.Add(text4);
         contentGrid.Children.Add(text5);
         contentGrid.Children.Add(text6);
         contentGrid.Children.Add(text7);

         contentGrid.Children.Add(value0);
         contentGrid.Children.Add(value1);
         contentGrid.Children.Add(value2);
         contentGrid.Children.Add(value3);
         contentGrid.Children.Add(value4);
         contentGrid.Children.Add(value5);
         contentGrid.Children.Add(value6);
         contentGrid.Children.Add(value7);
      }

      private static void PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
      {
         if (!(obj is TimeAxisControl control)) return;
         var start = (int)e.NewValue;

         control.text0.Text = FormatTime(start + 0);
         control.text1.Text = FormatTime(start + 1);
         control.text2.Text = FormatTime(start + 2);
         control.text3.Text = FormatTime(start + 3);
         control.text4.Text = FormatTime(start + 4);
         control.text5.Text = FormatTime(start + 5);
         control.text6.Text = FormatTime(start + 6);
         control.text7.Text = FormatTime(start + 7);
      }

      private static string FormatTime(int value)
      {
         var val = value.ToString();
         if (val.Length == 1)
         {
            val = "0" + val;
         }
         return val + ":00";
      }

      #endregion
   }
}
