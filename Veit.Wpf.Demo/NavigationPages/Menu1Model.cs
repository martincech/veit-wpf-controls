﻿using System;
using Veit.Wpf.Controls.Models;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Demo.NavigationPages
{
    public class Menu1Model : IMenuPage
    {
        public Menu1Model()
        {
            BarMenu = ModelHelper.GetUriFromType(typeof(Menu1BarMenuView));
            Source = ModelHelper.GetUriFromType(typeof(Menu1ContentView));
        }

        public bool IsOptional => false;
        public StockIcon Icon => StockIcon.Home;
        public Uri BarMenu { get; set; }
        public string Title => "Menu 1";
        public Uri Source { get; set; }
    }
}
