﻿using Veit.Wpf.Mvvm.Observable;
using System;
using System.Windows.Input;
using Veit.Wpf.Commands;
using Veit.Wpf.Controls.Models.Card;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Controls.ViewModels.Card
{
   public class CardBaseViewModel : ObservableObject, ICard
   {
      private ICommand switchToDetailCommand;
      protected StockIcon icon;
      private readonly string title;
      private readonly Action switchToDetailAction;

      public CardBaseViewModel(StockIcon icon, string title, Action switchToDetailAction)
      {
         this.icon = icon;
         this.title = title;
         this.switchToDetailAction = switchToDetailAction;
      }

      public string Title { get { return title; } }
      public StockIcon Icon { get { return icon; } set { SetProperty(ref icon, value); } }

      public ICommand SwitchToDetailCommand
      {
         get
         {
            return switchToDetailCommand ?? (switchToDetailCommand = new RelayCommand(
               () =>
               {
                  switchToDetailAction?.Invoke();
               },
               () => true
               ));
         }
      }

   }
}
