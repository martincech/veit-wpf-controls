﻿using System.Collections.Generic;
using Veit.Wpf.Controls.Models.Navigation;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Controls.ViewModels;
using Veit.Wpf.Controls.ViewModels.Navigation;
using Veit.Wpf.Demo.NavigationPages;

namespace Veit.Wpf.Demo
{
    /// <summary>
    /// Interaction logic for DemoNavigationWindow.xaml
    /// </summary>
    public partial class DemoNavigationWindow
    {
        public DemoNavigationWindow()
        {
            InitializeComponent();
            Loaded += DemoNavigationWindow_Loaded;
        }

        private void DemoNavigationWindow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            DataContext = new StandardNavigationViewModel(new NavigationService(), CreateDemoPages());
        }

        private static IEnumerable<PageViewModel> CreateDemoPages()
        {
            var model1 = new Menu1Model();
            var model2 = new Menu2Model();
            var modelOption = new MenuOptionModel();

            var mainMenuPages = new List<IMenuPage>
            {
                model1,
                model2
            };
            var optionMenuPages = new List<IMenuPage>
            {
                modelOption
            };


            return new List<PageViewModel>
            {
                new PageViewModel(new PageModel
                {
                    MainMenuItems = mainMenuPages,
                    OptionalMenuItems = optionMenuPages,
                    BarMenu = model1.BarMenu,
                    DefaultDetail = model1
                }),
                new PageViewModel(new PageModel
                {
                    MainMenuItems = mainMenuPages,
                    OptionalMenuItems = optionMenuPages,
                    BarMenu = model2.BarMenu,
                    DefaultDetail = model2
                }),
                new PageViewModel(new PageModel
                {
                    MainMenuItems = mainMenuPages,
                    OptionalMenuItems = optionMenuPages,
                    BarMenu = modelOption.BarMenu,
                    DefaultDetail = modelOption
                })
            };
        }
    }
}
