﻿using System;
using System.Collections.Generic;
using System.Linq;
using Veit.Wpf.Controls.Models.Page;

namespace Veit.Wpf.Controls.Models.Navigation
{
    public class NavigationItem : IEquatable<NavigationItem>
    {
        public INavigationPage Item { get; set; }
        public IEnumerable<IMenuPage> MainMenuItems { get; set; }
        public IEnumerable<IMenuPage> OptionalMenuItems { get; set; }
        public Uri BarMenu { get; set; }
        public int SelectedMenuIndex { get; set; }

        #region Equals

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((NavigationItem)obj);
        }

        public virtual bool Equals(NavigationItem other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Item.PageIsSame(other.Item) &&
                   SelectedMenuIndex == other.SelectedMenuIndex &&
                   PageCollectionIsSame(MainMenuItems, other.MainMenuItems) &&
                   PageCollectionIsSame(OptionalMenuItems, other.OptionalMenuItems) &&
                    (
                        BarMenu == other.BarMenu ||
                        BarMenu != null &&
                        BarMenu.Equals(other.BarMenu)
                    );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                if (Item != null)
                {
                    hashCode = hashCode * 59 + Item.GetHashCode();
                }
                hashCode = hashCode * 59 + SelectedMenuIndex.GetHashCode();
                if (MainMenuItems != null)
                    hashCode = hashCode * 59 + MainMenuItems.GetHashCode();
                if (OptionalMenuItems != null)
                    hashCode = hashCode * 59 + OptionalMenuItems.GetHashCode();
                if (BarMenu != null)
                    hashCode = hashCode * 59 + BarMenu.GetHashCode();
                return hashCode;
            }
        }


        private static bool PageCollectionIsSame(IEnumerable<INavigationPage> col1, IEnumerable<INavigationPage> col2)
        {
            if (col1 == null && col2 == null)
            {
                return true;
            }
            if (col1 == null || col2 == null)
            {
                return false;
            }

            var count = col1.Count();
            if (count != col2.Count()) return false;

            for (var i = 0; i < count; i++)
            {
                if (!col1.ElementAt(i).PageIsSame(col2.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Operators
#pragma warning disable 1591

        public static bool operator ==(NavigationItem left, NavigationItem right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(NavigationItem left, NavigationItem right)
        {
            return !Equals(left, right);
        }

#pragma warning restore 1591
        #endregion Operators
    }
}
