﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Data;

namespace Veit.Wpf.Converters
{
   /// <summary>
   /// Converter that chain multiple converters with its own parameter (separate with ",").
   /// Source: https://stackoverflow.com/a/48503354
   /// </summary>
   public class ValueConverterGroup : List<IValueConverter>, IValueConverter
   {
      private string[] parameters;

      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (parameter != null)
         {
            parameters = Regex.Split(parameter.ToString(), @"(?<!\\),");
         }
         return (this).Aggregate(value, (current, converter) => converter.Convert(current, targetType, GetParameter(converter), culture));
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }

      private string GetParameter(IValueConverter converter)
      {
         if (parameters == null)
         {
            return null;
         }

         var index = IndexOf(converter);
         string parameter;

         try
         {
            parameter = parameters[index];
         }
         catch (IndexOutOfRangeException)
         {
            parameter = null;
         }

         if (parameter != null)
         {
            parameter = Regex.Unescape(parameter);
         }
         return parameter;
      }
   }
}
