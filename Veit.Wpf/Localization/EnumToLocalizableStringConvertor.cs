﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Veit.Wpf.Localization
{
    public class EnumToLocalizableStringConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IEnumerable collection)
            {
                return (from object item in collection select LocalizeElement((Enum)item)).ToList();
            }
            return !(value is Enum val) ? value : LocalizeElement(val);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private static string LocalizeElement(Enum element)
        {
            return element.GetLocalizableName();
        }
    }
}
