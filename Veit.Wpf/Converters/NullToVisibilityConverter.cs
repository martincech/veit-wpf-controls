﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Veit.Wpf.Converters
{
   public sealed class NullToVisibilityConverter : IValueConverter
   {
      public NullToVisibilityConverter()
      {
         HideValue = Visibility.Hidden;
      }

      public Visibility HideValue { get; set; }

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var invert = IsInvertParameterSet(parameter);

         if (invert)
         { 
            return value == null ? Visibility.Visible : HideValue;
         }
         return value == null ? HideValue : Visibility.Visible;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }

      private static bool IsInvertParameterSet(object parameter)
      {
         var invertParameter = parameter as string;
         if (!string.IsNullOrEmpty(invertParameter) &&
             string.Equals(invertParameter, "invert", StringComparison.OrdinalIgnoreCase))
         {
            return true;
         }
         return false;
      }
   }
}