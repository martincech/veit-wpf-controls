﻿using System;
using System.Globalization;
using System.Windows.Data;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Converters
{
    public class IconFontToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return value == null ? null : IconResolver.GetTextFromIconFont((StockIcon)value);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
