﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Veit.Wpf.Controls.ViewModels.Calendar;

namespace Veit.Wpf.Converters
{
   public class TasksEventToCalendarItemConverter : IMultiValueConverter
   {
      public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
      {
         var viewModel = new EventSummaryViewModel();

         if (values == null || values.Count() != 2 ||
            !DateTime.TryParse(values[0].ToString(), out var timeStamp) ||
            !(values[1] is ITaskViewModel parentVm))
         {
            return viewModel;
         }

         var events = new ObservableCollection<TaskEventViewModel>();

         //check time stamp and select corresponding day events (if exists) from parentVm
         var itemMonth = timeStamp.Month;
         if (itemMonth == parentVm.DisplayMonth.Month)
         {
            events = parentVm.SelectedMonthEvents;
         }
         else
         {  //find events for item month
            var date = new DateTime(timeStamp.Year, timeStamp.Month, 1);
            events = parentVm.GetMonthlyEvents(date);
         }

         if (events == null) return viewModel;  //no events for whole month


         //find events for concrete day
         var dayEvents = events.Where(f => f.TimeStamp.Day == timeStamp.Day);
         viewModel.Events = new ObservableCollection<TaskEventViewModel>(dayEvents);

         return viewModel;
      }

      public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
