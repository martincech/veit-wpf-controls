﻿using System.Windows;

namespace Veit.Wpf.Controls.Views.Flyout
{
   /// <summary>
   /// Interaction logic for ErrorFlyoutView.xaml
   /// </summary>
   public partial class ErrorFlyoutView
   {
      public static readonly DependencyProperty TextProperty = DependencyProperty.Register(nameof(Text), typeof(string), typeof(ErrorFlyoutView), new PropertyMetadata(string.Empty));
      public string Text
      {
         get { return (string)GetValue(TextProperty); }
         set { SetValue(TextProperty, value); }
      }

      public ErrorFlyoutView()
      {
         InitializeComponent();
      }
   }
}
