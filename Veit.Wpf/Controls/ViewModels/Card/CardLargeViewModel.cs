﻿using System;
using Veit.Wpf.Controls.Models.Card;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Wpf.Controls.ViewModels.Card
{
   public class CardLargeViewModel : CardBaseViewModel, ICardLarge
   {
      private string currentLabel;
      private string scheduleLabel;
      private string finishLabel;

      private int currentValue;
      private int scheduleValue;
      private int finishValue;

      public CardLargeViewModel(StockIcon icon, string title, Action switchToDetailAction)
         : base(icon, title, switchToDetailAction)
      {
      }

      public string CurrentLabel { get { return currentLabel; } set { SetProperty(ref currentLabel, value); } }
      public string ScheduleLabel { get { return scheduleLabel; } set { SetProperty(ref scheduleLabel, value); } }
      public string FinishLabel { get { return finishLabel; } set { SetProperty(ref finishLabel, value); } }


      public int CurrentValue { get { return currentValue; } set { SetProperty(ref currentValue, value); } }
      public int ScheduleValue { get { return scheduleValue; } set { SetProperty(ref scheduleValue, value); } }
      public int FinishValue { get { return finishValue; } set { SetProperty(ref finishValue, value); } }
   }
}
