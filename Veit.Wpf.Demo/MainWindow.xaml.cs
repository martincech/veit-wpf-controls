﻿namespace Veit.Wpf.Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
   {
      public MainWindow()
      {
         InitializeComponent();
      }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoNavigationWindow();
            win.Show();
        }

        private void Button_Click_1(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoDataGridTouchOptimizedWindow();
            win.Show();
        }

        private void Button_Click_2(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoDataGridPagingWindow();
            win.Show();
        }

        private void Button_Click_3(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoCardWindow();
            win.Show();
        }

        private void Button_Click_4(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoButtonWindow();
            win.Show();
        }

        private void Button_Click_5(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoFlyoutWindow();
            win.Show();
        }

        private void Button_Click_6(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoTextBlockWindow();
            win.Show();
        }

        private void Button_Click_7(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoMiscWindow();
            win.Show();
        }

        private void Button_Click_8(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoColorsWindow();
            win.Show();
        }

        private void Button_Click_9(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoWizardWindow();
            win.Show();
        }

        private void Button_Click_10(object sender, System.Windows.RoutedEventArgs e)
        {
            var win = new DemoIconsWindow();
            win.Show();
        }
    }
}
