﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Veit.Wpf.Cursor
{
    public class CursorScopeGuard : IDisposable
    {
        public CursorScopeGuard(CursorType cursorType)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Mouse.OverrideCursor = cursorType.Map();
            });
        }

        protected virtual void Dispose(bool disposing)
        {
            ResetCursor();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void ResetCursor()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Mouse.OverrideCursor = null;
            });
        }
    }

    public sealed class WaitCursorScopeGuard : CursorScopeGuard
    {
        public WaitCursorScopeGuard()
           : base(CursorType.Wait)
        {
        }
    }
}
