﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Veit.Wpf.Mvvm.Observable;
using Xunit;

namespace MvvmFramework.Tests.UnitTests
{
   public class ValidatableObservableObjectTests
   {
        private readonly StubValidatableObservableObject obj;
        private bool raised;

      public ValidatableObservableObjectTests()
      {
         obj = new StubValidatableObservableObject();
         raised = false;
      }

      [Fact]
      public void RequiredAttributeDataErrorIsRaised()
      {
         obj.ErrorsChanged += (sender, e) =>
         {
            raised = true;
         };

         obj.Validate();
         Assert.True(raised, "ErrorsChanged never invoked");
      }

      [Fact]
      public void MaximumLengthAttributeDataErrorIsRaised()
      {
         obj.ErrorsChanged += (sender, e) =>
         {
            Assert.True(e.PropertyName == "MaxLengthProperty");
            raised = true;
         };
         obj.MaxLengthProperty = "lasjfůlkasjflkajflkjaslkfd";
         Assert.True(raised, "ErrorsChanged never invoked");
         Assert.True(obj.HasErrors);
         var errors = obj.GetErrors("MaxLengthProperty");
         var validationResults = errors as ValidationResult[] ?? errors.ToArray();

         Assert.True(validationResults.Count() == 1, "More errors returned where single should be");
         Assert.True(validationResults.Any(p => p.ErrorMessage == "Too lengthy error"));
      }

      [Fact]
      public void CustomRulesErrorsDataErrorIsRaised()
      {
         obj.ErrorsChanged += (sender, e) =>
         {
            Assert.True(e.PropertyName == "CustomValidationProperty");
            raised = true;
         };
         obj.CustomValidationProperty = "";
         Assert.True(raised, "ErrorsChanged never invoked");
         Assert.True(obj.HasErrors);
         var errors = obj.GetErrors("CustomValidationProperty");
         var validationResults = errors as ValidationResult[] ?? errors.ToArray();

         Assert.True(validationResults.Count() == 1, "More errors returned where single should be");
         Assert.True(validationResults.Any(p => p.ErrorMessage == "Too short error"));
      }

      [Fact]
      public void CustomRules_ErrorsRemoved_OnConnectedProperties()
      {
         Assert.True(!obj.HasErrors);
         obj.From = new DateTime(2014, 1, 1, 5, 0, 0);
         obj.To = new DateTime(2014, 1, 1, 4, 0, 0);
         Assert.True(obj.GetErrors("From").Any());
         Assert.True(obj.GetErrors("To").Any());
         Assert.True(obj.HasErrors);

         obj.To = new DateTime(2014, 1, 1, 8, 0, 0);
         Assert.True(!obj.GetErrors("From").Any());
         Assert.True(!obj.GetErrors("To").Any());
         Assert.True(!obj.HasErrors);

         obj.From = new DateTime(2014, 1, 1, 9, 0, 0);
         Assert.True(obj.GetErrors("From").Any());
         Assert.True(!obj.GetErrors("To").Any());
         Assert.True(obj.HasErrors);

         obj.To = new DateTime(2014, 1, 1, 10, 0, 0);
         Assert.True(!obj.GetErrors("From").Any());
         Assert.True(!obj.GetErrors("To").Any());
         Assert.True(!obj.HasErrors);

      }
   }

   internal class StubValidatableObservableObject : ValidatableObservableObject
   {
      private string requiredProperty;
      private string maxLengthProperty;
      private string customValidationProperty;
      private DateTime from;
      private DateTime to;

      [Required]
      public string RequiredProperty
      {
         get { return requiredProperty; }
         set { SetPropertyAndValidate(ref requiredProperty, value); }
      }

      [StringLength(4, ErrorMessage = "Too lengthy error")]
      public string MaxLengthProperty
      {
         get { return maxLengthProperty; }
         set { SetPropertyAndValidate(ref maxLengthProperty, value); }
      }

      public string CustomValidationProperty
      {
         get { return customValidationProperty; }
         set { SetPropertyAndValidate(ref customValidationProperty, value); }
      }

      public DateTime From
      {
         get { return from; }
         set { SetPropertyAndValidate(ref from, value); }
      }

      public DateTime To
      {
         get { return to; }
         set { SetPropertyAndValidate(ref to, value); }
      }

      protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         if (propertyName.Equals(nameof(CustomValidationProperty)))
         {
            var stringValue = value as string;
            if (string.IsNullOrEmpty(stringValue))
            {
               return new[]
               {
                  new ValidationResult("Too short error", new []{propertyName})
               };
            }
         }

         if ((propertyName.Equals(nameof(From)) && DateTime.Compare((DateTime)value, To) > 0) ||
             (propertyName.Equals(nameof(To)) && DateTime.Compare(From, (DateTime)value) > 0))
         {
            return new[]
            {
               new ValidationResult("From time must be before To time", new[]{ nameof(From), nameof(To)})
            };
         }
         return base.AdditionalValidationRules(value, propertyName);
      }
   }
}
