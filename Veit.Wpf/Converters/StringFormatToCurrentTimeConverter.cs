﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Veit.Wpf.Converters
{
   public class StringFormatToCurrentTimeConverter : IValueConverter, IMultiValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         if( value == null || !(value is string))
         {
            return null;
         }

         if (parameter == null || !(parameter is DateTime timeStamp))
         {
            return DateTime.Now.ToString(value.ToString());
         }
         return timeStamp.ToString(value.ToString());
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }

      public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
      {
         if (values.Length < 2)
         {
            return null;
         }
         return Convert(values[1], targetType, values[0], culture);
      }

      public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
