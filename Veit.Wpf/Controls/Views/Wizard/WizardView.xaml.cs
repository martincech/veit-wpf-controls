﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using Veit.Wpf.Controls.Models;
using Veit.Wpf.Controls.ViewModels;
using Veit.Wpf.Controls.ViewModels.Wizard;

namespace Veit.Wpf.Controls.Views.Wizard
{
   /// <summary>
   /// Interaction logic for WizardView.xaml
   /// </summary>
   public partial class WizardView : UserControl
   {
      public static readonly DependencyProperty ContainerProperty = DependencyProperty.Register(nameof(Container), typeof(ObservableCollection<FrameworkElement>), typeof(WizardView), new PropertyMetadata(new ObservableCollection<FrameworkElement>()));

      public ObservableCollection<FrameworkElement> Container
      {
         get { return (ObservableCollection<FrameworkElement>)GetValue(ContainerProperty); }
         set { SetValue(ContainerProperty, value); }
      }


      public WizardView()
      {
         InitializeComponent();
         Loaded += WizardView_Loaded;
         Unloaded += WizardView_Unloaded;
      }

      #region Private helpers

      private void WizardView_Unloaded(object sender, RoutedEventArgs e)
      {
         if (Container != null)
         {
            Container.Clear();
         }
      }

      private void WizardView_Loaded(object sender, RoutedEventArgs e)
      {
         if (!(DataContext is WizardViewModel vm))
         {
            return;
         }
         vm.StepCount = contentBox.Items.Count;
         vm.PropertyChanged += Vm_PropertyChanged;

         foreach (var item in Container)
         {
            item.DataContext = vm.ContentViewModel;
         }
      }

      private void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
      {
         if (!(DataContext is ErrorValidationViewModel vm))
         {
            return;
         }

         if (e.PropertyName.Equals(nameof(vm.IsValidationError)) && vm.IsValidationError)
         {
            if (!(Window.GetWindow(this) is IErrorView window))
            {
               throw new System.NotSupportedException($"Current window cannot display error message, because it is not implemented interface: {nameof(IErrorView)}");
            }
            window.DisplayError(vm.ValidationErrorText);
         }
      }

      #endregion
   }
}
