﻿using Veit.Wpf.Demo.Data;

namespace Veit.Wpf.Demo
{
    /// <summary>
    /// Interaction logic for DemoDataGridTouchOptimizedWindow.xaml
    /// </summary>
    public partial class DemoDataGridTouchOptimizedWindow
    {
        public DemoDataGridTouchOptimizedWindow()
        {
            InitializeComponent();
            dataGrid.ItemsSource = new DataGridSource().Source;
        }
    }
}
