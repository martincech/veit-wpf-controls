﻿using System;
using System.Collections.Generic;
using System.Linq;
using Veit.Wpf.Controls.Models.Navigation;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Controls.Views.Navigation;
using static Veit.Wpf.Controls.ViewModels.PageViewModel;

namespace Veit.Wpf.Controls.ViewModels.Navigation
{
    public class StandardNavigationViewModel : NavigationViewModel, INavigationChanged
    {
        #region Private fields

        private PageViewModel currentPageViewModel;
        private readonly List<PageViewModel> pages;
        private int selectedIndex;
        private int selectedOptionIndex;

        #endregion

        #region Public interfaces

        public StandardNavigationViewModel(NavigationService navService, IEnumerable<PageViewModel> pages)
           : base(navService)
        {
            if (pages == null) return;
            this.pages = new List<PageViewModel>(pages);
            SetInitialPage();
        }


        public PageViewModel CurrentPageViewModel
        {
            get { return currentPageViewModel; }
            set { SetProperty(ref currentPageViewModel, value); }
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set { SetProperty(ref selectedIndex, value); }
        }
        public int SelectedOptionIndex { get { return selectedOptionIndex; } set { SetProperty(ref selectedOptionIndex, value); } }



        public void SelectionChanged(int index, bool isOptional)
        {
            var newSelectedPage = !isOptional ? CurrentPageViewModel.MainMenuItems[index] : CurrentPageViewModel.MainMenuOptionItems[index];

            UpdateCurrentPage(newSelectedPage, newSelectedPage.BarMenu);
            NavigateTo(CurrentPageViewModel.Map());
        }

        /// <summary>
        /// Change detail on current page (without change menu selected item)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="barMenu"></param>
        public void ShowSubContent(Uri source, Uri barMenu = null)
        {
            var selectedIndexBackup = SelectedIndex;
            var selectedOptionIndexBackup = SelectedOptionIndex;

            var newPage = CopyCurrentPage();
            newPage.DetailPage.Source = source;
            if (barMenu != null)
            {
                newPage.BarMenu = barMenu;
            }

            CurrentPageViewModel = newPage;
            NavigateTo(CurrentPageViewModel.Map());

            SelectedOptionIndex = selectedOptionIndexBackup;
            SelectedIndex = selectedIndexBackup;
        }

        #endregion

        #region Private helpers

        private void SetInitialPage()
        {
            CurrentPageViewModel = pages.FirstOrDefault();
            if (CurrentPageViewModel != null)
            {
                SelectionChanged(0, false);
            }
        }

        private PageViewModel CopyCurrentPage()
        {
            var model = new PageModel
            {
                MainMenuItems = CurrentPageViewModel.MainMenuItems,
                OptionalMenuItems = CurrentPageViewModel.MainMenuOptionItems,
                BarMenu = CurrentPageViewModel.BarMenu,
                DefaultDetail = new NavigationPage
                {
                    Title = CurrentPageViewModel.DetailPage.Title,
                    Source = CurrentPageViewModel.DetailPage.Source
                }
            };
            return new PageViewModel(model);
        }


        private void UpdateCurrentPage(INavigationPage page, Uri barMenu)
        {
            //find new source in root pages
            var newRoot = pages.FirstOrDefault(f => f.DetailPage.Title.Equals(page.Title));
            if (newRoot == null)
            {
                throw new FieldAccessException($"Page: {page.Title} is not found in existed pages");
            }

            //set new root
            newRoot.DetailPage = new NavigationPage
            {
                Title = page.Title,
                Source = page.Source
            };
            newRoot.BarMenu = barMenu;
            CurrentPageViewModel = newRoot;

            //set selected index in new root page (new page can have different main menu -> selected item must be re-indexed)
            var newIndex = newRoot.DefaultSelectedMenuIndex;
            if (newIndex < CurrentPageViewModel.MainMenuItems.Count)
            {  //main menu
                SelectedOptionIndex = -1;
                SelectedIndex = newIndex;
            }
            else
            {  //option menu
                SelectedIndex = -1;
                SelectedOptionIndex = newIndex - CurrentPageViewModel.MainMenuItems.Count;
            }
        }


        protected override void Navigated(NavigationItem page)
        {
            UpdateCurrentPage(page.Item, page.BarMenu);
        }

        #endregion
    }
}
