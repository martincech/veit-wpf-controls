﻿using MahApps.Metro.Controls;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;
using Veit.Wpf.Controls.Models.Page;

namespace Veit.Wpf.Converters
{
    public class MenuItemToHamburgerMenuIconItemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !(value is IEnumerable collection)) return null;

            var items = new ObservableCollection<HamburgerMenuIconItem>();
            foreach (var item in collection)
            {
                if (!(item is IMenuPage page)) continue;
                items.Add(new HamburgerMenuIconItem
                {
                    Label = page.Title,
                    Tag = page.Source,
                    Icon = page.Icon
                });
            }
            return items;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
