﻿using System.Collections.ObjectModel;

namespace Veit.Wpf.Demo.Data
{
    public class DataGridSource
    {
        public DataGridSource()
        {
            Source = new ObservableCollection<DataGridModel>
            {
                GetModel("Sensor 01", "output", 999),
                GetModel("Sensor 02", "in house", 52),
                GetModel("Sensor 03", "in house", 2),
                GetModel("Sensor 04", "in house", 40),
                GetModel("Sensor 05", "output", 500),
                GetModel("Sensor 06", "output", 550),
                GetModel("Scale 001", "BAT1", 1550),
                GetModel("Scale 002", "BAT1", 1492),
                GetModel("Scale 002", "BAT1", 1220),
            };
        }

        private static DataGridModel GetModel(string name, string description, int value)
        {
            return new DataGridModel
            {
                Name = name,
                Description = description,
                Value = value
            };
        }

        public ObservableCollection<DataGridModel> Source { get; set; }
    }

    public class DataGridModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Value { get; set; }
    }
}
