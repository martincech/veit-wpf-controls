﻿using System;

namespace Veit.Wpf.Controls.Models.Page
{
   public interface INavigationPage
   {
      string Title { get; }
      Uri Source { get; set; }
   }
}
