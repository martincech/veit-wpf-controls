﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Veit.Wpf.Mvvm.Observable;
using Xunit;

namespace MvvmFramework.Tests.UnitTests
{
   public class RecursiveValidatableObservableObjectTests
   {
        private readonly StubRecursiveValidatableObservableObject obj;
        private bool raised;
      public RecursiveValidatableObservableObjectTests()
      {
         obj = new StubRecursiveValidatableObservableObject();
         raised = false;
      }

      [Fact]
      public void InnerObservablePropertyChangedEventHandlerIsRaised()
      {
         obj.ObservableProperty = new StubObservableObject();

         obj.PropertyChanged += (sender, e) =>
         {
            Assert.Equal("ObservableProperty", e.PropertyName);
            raised = true;
         };

         obj.ObservableProperty.ChangedProperty = "some value";
         Assert.True(raised, "PropertyChanged some value never raised");
         raised = false;

         obj.ObservableProperty.ChangedProperty = "some value";
         Assert.True(!raised, "PropertyChanged same some value raised");
         raised = false;

         obj.ObservableProperty.ChangedProperty = null;
         Assert.True(raised, "PropertyChanged null value never raised");
         raised = false;

         obj.ObservableProperty.ChangedProperty = "";
         Assert.True(raised, "PropertyChanged empty value never raised");
         raised = false;
      }

      [Fact]
      public void InnerValidatableObservablePropertyDataErrorIsRaised()
      {
         obj.ValidatableObservableProperty = new StubValidatableObservableObject();

         obj.ErrorsChanged += (sender, e) =>
         {
            Assert.Equal("ValidatableObservableProperty", e.PropertyName);
            raised = true;
         };

         // check if errors raised
         obj.ValidatableObservableProperty.MaxLengthProperty = "lasjfůlkasjflkajflkjaslkfd";
         Assert.True(raised, "ErrorsChanged never invoked");
         Assert.True(obj.HasErrors, "Parent has no errors");
         var errors = obj.GetErrors("ValidatableObservableProperty");
         var validationResults = errors as ValidationResult[] ?? errors.ToArray();

         Assert.True(validationResults.Count() == 1, "More errors returned where single should be");
         Assert.True(validationResults.Any(p => p.ErrorMessage == "Too lengthy error"));


         // check if errors cleared
         raised = false;
         obj.ValidatableObservableProperty.MaxLengthProperty = "123";
         Assert.True(raised, "ErrorsChanged after valid property never invoked");
         Assert.True(!obj.HasErrors, "Parent has errors");
         errors = obj.GetErrors("ValidatableObservableProperty");
         validationResults = errors as ValidationResult[] ?? errors.ToArray();

         Assert.True(!validationResults.Any(), "More errors returned where none should be");
      }
   }

   internal class StubRecursiveValidatableObservableObject : RecursiveValidatableObservableObject
   {
      private StubObservableObject observableProperty;
      private StubValidatableObservableObject validatableObservableProperty;

      public StubObservableObject ObservableProperty
      {
         get { return observableProperty; }
         set { SetProperty(ref observableProperty, value); }
      }

      public StubValidatableObservableObject ValidatableObservableProperty
      {
         get { return validatableObservableProperty; }
         set { SetPropertyAndValidate(ref validatableObservableProperty, value); }
      }
   };
}
