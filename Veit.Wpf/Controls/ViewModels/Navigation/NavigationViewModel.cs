﻿using System;
using System.Windows;
using System.Windows.Input;
using Veit.Wpf.Commands;
using Veit.Wpf.Controls.Models.Navigation;

namespace Veit.Wpf.Controls.ViewModels.Navigation
{
    public abstract class NavigationViewModel : ErrorValidationViewModel
    {
        private ICommand goBackCommand;
        private readonly NavigationService navigationService;

        protected NavigationViewModel(NavigationService navigationService)
        {
            this.navigationService = navigationService;
            RaisePropertyChanged(() => CanGoBack);
            this.navigationService.CurrentPageChanged += NavigationService_CurrentPageChanged;
        }


        public bool CanGoBack { get { return navigationService.CanGoBack; } }

        public ICommand GoBackCommand
        {
            get
            {
                return goBackCommand ?? (goBackCommand = new RelayCommand(
                   () =>
                   {
                       GoBackRoutine();
                   },
                   () => true
                   ));
            }
        }


        public void NavigateTo(NavigationItem page)
        {
            if (page == null) return;
            navigationService.CurrentPageChanged -= NavigationService_CurrentPageChanged;
            navigationService.NavigateTo(page);
            RaisePropertyChanged(() => CanGoBack);
            navigationService.CurrentPageChanged += NavigationService_CurrentPageChanged;
        }

        private void NavigationService_CurrentPageChanged(object sender, NavigationItem e)
        {
            Navigated(e);
        }

        private void GoBackRoutine()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (CanGoBack)
                {
                    navigationService.GoBack();
                    RaisePropertyChanged(() => CanGoBack);
                }
            }));
        }

        protected abstract void Navigated(NavigationItem page);
    }
}
