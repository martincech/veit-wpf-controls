﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Veit.Wpf.Controls.Views
{
   /// <summary>
   /// Interaction logic for PagingControlsView.xaml
   /// </summary>
   public partial class PagingControlsView : UserControl
   {
      #region Dependency properties

      public static readonly DependencyProperty CurrentItemProperty = DependencyProperty.Register(nameof(CurrentItem), typeof(int), typeof(PagingControlsView), new PropertyMetadata(0));
      public static readonly DependencyProperty ItemsCountProperty = DependencyProperty.Register(nameof(ItemsCount), typeof(int), typeof(PagingControlsView), new PropertyMetadata(0));
      public static readonly DependencyProperty CanPreviousProperty = DependencyProperty.Register(nameof(CanPrevious), typeof(bool), typeof(PagingControlsView), new PropertyMetadata(false));
      public static readonly DependencyProperty CanNextProperty = DependencyProperty.Register(nameof(CanNext), typeof(bool), typeof(PagingControlsView), new PropertyMetadata(false));
      public static readonly DependencyProperty PreviousItemCommandProperty = DependencyProperty.Register(nameof(PreviousItemCommand), typeof(ICommand), typeof(PagingControlsView), new PropertyMetadata(null));
      public static readonly DependencyProperty NextItemCommandProperty = DependencyProperty.Register(nameof(NextItemCommand), typeof(ICommand), typeof(PagingControlsView), new PropertyMetadata(null));

      public int CurrentItem
      {
         get { return (int)GetValue(CurrentItemProperty); }
         set { SetValue(CurrentItemProperty, value); }
      }

      public int ItemsCount
      {
         get { return (int)GetValue(ItemsCountProperty); }
         set { SetValue(ItemsCountProperty, value); }
      }

      public bool CanPrevious
      {
         get { return (bool)GetValue(CanPreviousProperty); }
         set { SetValue(CanPreviousProperty, value); }
      }

      public bool CanNext
      {
         get { return (bool)GetValue(CanNextProperty); }
         set { SetValue(CanNextProperty, value); }
      }

      public ICommand PreviousItemCommand
      {
         get { return (ICommand)GetValue(PreviousItemCommandProperty); }
         set { SetValue(PreviousItemCommandProperty, value); }
      }

      public ICommand NextItemCommand
      {
         get { return (ICommand)GetValue(NextItemCommandProperty); }
         set { SetValue(NextItemCommandProperty, value); }
      }

      #endregion

      public PagingControlsView()
      {
         InitializeComponent();
      }
   }
}
